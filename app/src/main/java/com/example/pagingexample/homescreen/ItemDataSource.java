package com.example.pagingexample.homescreen;

import android.arch.paging.PageKeyedDataSource;
import android.support.annotation.NonNull;


import com.example.pagingexample.pojo.IMResponse;
import com.example.pagingexample.pojo.Result;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ItemDataSource extends PageKeyedDataSource<Integer, Result> {

    public static final int PAGE_SIZE = 50;
    private static final int FIRST_PAGE = 1;
    private final String API_KEY = "f200ea93d28d03201a0e1caee1ebd3e6";

    @Override
    public void loadInitial(@NonNull final LoadInitialParams<Integer> params, @NonNull final LoadInitialCallback<Integer, Result> callback) {

        RetrofitClient.getInstance()
                .getApi().getAllImages(API_KEY, FIRST_PAGE)
                .enqueue(new Callback<IMResponse>() {
                    @Override
                    public void onResponse(Call<IMResponse> call, Response<IMResponse> response) {
                        if (response.body() != null) {
                            callback.onResult(response.body().getResults(), null, FIRST_PAGE + 1);
                    }
                    }
                    @Override
                    public void onFailure(Call<IMResponse> call, Throwable t) {
                    }
                });
    }

    @Override
    public void loadBefore(@NonNull final LoadParams<Integer> params, @NonNull final LoadCallback<Integer, Result> callback) {
        RetrofitClient.getInstance()
                .getApi().getAllImages(API_KEY, params.key)
                .enqueue(new Callback<IMResponse>() {
                    @Override
                    public void onResponse(Call<IMResponse> call, Response<IMResponse> response) {
                        Integer adjacentKey = (params.key > 1) ? params.key - 1 : null;
                        if (response.body() != null) {
                            callback.onResult(response.body().getResults(), adjacentKey);
                        }
                    }

                    @Override
                    public void onFailure(Call<IMResponse> call, Throwable t) {

                    }
                });
    }

    @Override
    public void loadAfter(@NonNull final LoadParams<Integer> params, @NonNull final LoadCallback<Integer, Result> callback) {
        RetrofitClient.getInstance()
                .getApi()
                .getAllImages(API_KEY, params.key)
                .enqueue(new Callback<IMResponse>() {
                    @Override
                    public void onResponse(Call<IMResponse> call, Response<IMResponse> response) {
                        System.out.println(params.key);
                        if (response.body() != null && true) {
                            callback.onResult(response.body().getResults(), params.key + 1);
                        }
                    }

                    @Override
                    public void onFailure(Call<IMResponse> call, Throwable t) {

                    }
                });
    }
}
