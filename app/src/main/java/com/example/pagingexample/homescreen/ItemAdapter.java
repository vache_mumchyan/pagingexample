package com.example.pagingexample.homescreen;

import android.arch.paging.PagedListAdapter;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.example.pagingexample.R;
import com.example.pagingexample.pojo.Result;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.Objects;

public class ItemAdapter extends PagedListAdapter<Result, ItemAdapter.ItemViewHolder> {

    private String IMAGE_BASE_URL = "https://image.tmdb.org/t/p/w500";


    private static DiffUtil.ItemCallback<Result> DIFF_CALLBACK = new DiffUtil.ItemCallback<Result>() {
                @Override
                public boolean areItemsTheSame(Result oldItem, Result newItem) {
                    return oldItem.getId() == newItem.getId();
                }

                @Override
                public boolean areContentsTheSame(Result oldItem, Result newItem) {
                    return oldItem.equals(newItem);
                }
            };
    private Context mCtx;

    public ItemAdapter(Context mCtx) {
        super(DIFF_CALLBACK);
        this.mCtx = mCtx;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mCtx).inflate(R.layout.image_layout, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        final Result image = getItem(position);
        holder.userName.setText(Objects.requireNonNull(image).getTitle());
     //   holder.like.setText(image.getLikes().toString());
 //       holder.comments.setText(image.getComments().toString());

        Picasso.get()
                .load(IMAGE_BASE_URL+image.getPosterPath())
                .placeholder(R.drawable.shimmer_background)
                .into(holder.image);

        Transformation transformation = new RoundedTransformationBuilder()
                .borderColor(Color.BLACK)
                .borderWidthDp(1)
                .cornerRadiusDp(30)
                .oval(false)
                .build();

//        if (!image.getPosterPath().isEmpty()) {
//            Picasso.get()
//                    .load(IMAGE_BASE_URL+image.getPosterPath())
//                    .fit()
//                    .transform(transformation)
//                    .into(holder.userImage);
//        }
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {

        TextView userName, like, comments;
        ImageView image, userImage;

        public ItemViewHolder(View view) {
            super(view);
            image = view.findViewById(R.id.image);
            userImage = view.findViewById(R.id.user_image);
            userName = view.findViewById(R.id.user);
            like = view.findViewById(R.id.like);
            comments = view.findViewById(R.id.comments);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(mCtx, ""+(getAdapterPosition()), Toast.LENGTH_SHORT).show();
                }
            });

        }
    }
}
