package com.example.pagingexample.homescreen;

import com.example.pagingexample.pojo.IMResponse;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ImageServices {

    @GET("movie/popular")
    Call<IMResponse> getAllImages(@Query("api_key") String apiKey, @Query("page") int page);

    @GET("api/")
    Call<IMResponse> searchImages(@Query("key") String apiKey, @Query("q") String searchQuery);

}
