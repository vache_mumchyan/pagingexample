package com.example.pagingexample.Util;

public interface NetworkStatus {

    boolean isOnline();
}
